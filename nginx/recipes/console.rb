include_recipe "nginx::default"


template "/etc/nginx/conf.d/console.conf" do |_|
	action :create
	source "console.conf.erb"
	variables({
		:service_name => "console",
		:service_port => 3000
	})
end
