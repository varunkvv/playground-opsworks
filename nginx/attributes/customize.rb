normal[:nginx][:log_dir] = "/mnt/log/nginx"
default[:nginx][:keepalive] = 'on'
default[:nginx][:keepalive_timeout] = 65

default[:nginx][:worker_processes] = '1'
default[:nginx][:server_names_hash_bucket_size] = 64
