include_recipe "apt"
repo = 'http://ppa.launchpad.net/chris-lea/node.js/ubuntu'
packages = %w{ nodejs }
apt_repository 'node.js' do
	uri repo
	distribution node['lsb']['codename']
	components ['main']
	keyserver "hkp://keyserver.ubuntu.com:80"
	key "C7917B12"
	action :add
end

packages.each do |node_pkg|
	package node_pkg
end
