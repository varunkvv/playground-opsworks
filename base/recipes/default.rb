#create the top level log directory
directory node[:log_dir] do
	mode '0666'
	owner 'root'
	group 'root'
	action :create
end
